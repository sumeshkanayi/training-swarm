#!/bin/bash
#Allow all traffic
#Add /etc/hosts entry for each instance
# Add /etc/hosts entry in clients as well
apt install -y glusterfs-server
mkdir -p /gluster
systemctl restart glusterd
mkfs.ext4 /dev/xvdb
mount -t /dev/xvdb /gluster
mkdir /gluster/brick