**Pre requisites:**

 - Internet connection
 - vagrant
 - virtualbox or vmware fusion etc
 
**Steps:**


		cd vm && vagrant up
	    vagrant ssh node1

 - create first swarm manager

	 

       docker swarm init

 - make a note of token and commands to make them additional managers and workers etc

 - vagrant ssh node 2 and vagrant ssh node 3

	 - make them additional swarm managers

  

 - vagrant ssh to node4 , node 5 and node 6

  

	 - make them swarm workers

  

 - vagrant ssh to node 7 , node 8 and node 9

  

 - make them gluster nodes

  

 

       gluster peer probe node7 node8 node9

  

 - create volume

  

 

		    gluster volume create postgres replica 3 node7:/gluster-storage/bricks node8:/gluster-storage/bricks node9:/gluster-storage/bricks force
		    gluster volume start postgres

  

 - vagrant ssh to node 4, 5 and 6

  
		mount -t glusterfs node7:/postgres /gluster-mount

		mkdir /gluster-mount/postgres

		mkdir /gluster-mount/pg-admin

	    docker stack deploy -c wp.yml wp


